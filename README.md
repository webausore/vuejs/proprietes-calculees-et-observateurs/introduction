# VueJS - Propriétés calculées (computed) et observateurs (watch)

## Détails

-   Propriétés calculées :
    -   Cours : https://fr.vuejs.org/v2/guide/computed.html#Proprietes-calculees
    -   Vidéo : https://vueschool.io/lessons/vuejs-computed-properties?friend=vuejs
-   Observateurs :
    -   Cours : https://fr.vuejs.org/v2/guide/computed.html#Observateurs

Exercice :

-   Computed :
    -   Trouver un exemple d'utilisation de computed
    -   Montrer différence entre method et computed (mise en cache)
        Exercice : utiliser cette api https://jsonplaceholder.typicode.com/users et créer deux listes randomisées d'utilisateurs
        Créer la première avec computed
        Créer la deuxieme avec method
        Champ input en dessous pour ajouter un nouvel utilisateur a la liste
        Voir que computed change tout en refaisant un random alors que la method ne change pas
    -   Faire un exemple de computed avec mutateur (set)
        Exercice : montrer qu'on ne peut pas muter un computed par défaut
        Essayer ensuite de définir un mutateur et réessayer
        Reprendre l'exemple du fullname de vuejs
-   Watch :
    -   Exemple d'utilisation de watch
        Faire un scrap google (expliquer sur l'exercice ce qu'est un scrap)
        Greffer sur variable search un watch permettant de réaliser un setTimeOut afin de ne pas appeler la nouvelle recherche trop vite (un appel api quand l'utilisateur n'écrit plus au clavier pendant une seconde : pendant cette seconde : afficher un message "Recherche de résultat)
